import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class Departure extends Component {

  static propTypes = {
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
  };

  constructor(props){
    super();
    this.state= {
      departure: ''
    }
  }

  componentWillReceiveProps(departureInput) {
    this.setState({
      departure: departureInput.value
    });
  }

  onInputChange = (event) => {
    const departure = event.target.value;
    const name = this.props.name;
    this.setState({
      departure
    });

    this.props.onChange({
      name,
      departure
    });
  }

  render() {
    return (
      <input
        className='inputs'
        type="text"
        name='departureInput'
        placeholder= {this.props.placeholder}
        value={this.state.departure}
        onChange={this.onInputChange}
        required
        />
    );
  }
}

export default Departure;
