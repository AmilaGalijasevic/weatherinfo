import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import axios from 'axios';
import ShowDestinationWeatherInfo from './ShowDestinationWeatherInfo.js';

class GetDestinationWeatherInfo extends Component {

  static propTypes = {
    destinationLocation: PropTypes.object,
  }

  constructor(props) {
      super();
      this.state = {
        destinationLocation: {
          lat:'',
          lng:''
        },
        destinationWeatherInfo: {
          dewPoint:'',
          humidity: '',
          temerature: '',
          fog: '',
          cloudiness:'',
          highClouds: '',
          lowClouds: '',
          mediumClouds: '',
          windSpeed: ''
        },
    }
    this.getDestinationInfo= this.getDestinationInfo.bind(this);

  }

  componentWillReceiveProps(destLocation){
    const destLoc = destLocation;
    this.setState({
      destinationLocation: {
        lat:destLoc.destinationLocation.lat,
        lng:destLoc.destinationLocation.lng
      },
    });
    this.getDestinationInfo();
  };

  getDestinationInfo =() => {
    setTimeout(() => {
    var self= this;
    const destinationLocation = this.state.destinationLocation;
    var res;
    var parseString = require('xml2js').parseString;

    if(!destinationLocation.lat==''){
    axios.get(' https://api.met.no/weatherapi/locationforecast/1.9/?lat=' + destinationLocation.lat + '&lon='+ destinationLocation.lng)
    .then(function (response) {
    var xml = response.data
    parseString(xml, function (err, result) {
    res = result;
    });
      self.setState({
        destinationWeatherInfo: {
          dewPoint:res.weatherdata.product[0].time[0].location[0].dewpointTemperature[0].$.value,
          humidity: res.weatherdata.product[0].time[0].location[0].humidity[0].$.value,
          temperature: res.weatherdata.product[0].time[0].location[0].temperature[0].$.value,
          fog: res.weatherdata.product[0].time[0].location[0].fog[0].$.percent,
          cloudiness: res.weatherdata.product[0].time[0].location[0].cloudiness[0].$.percent,
          highClouds: res.weatherdata.product[0].time[0].location[0].highClouds[0].$.percent,
          lowClouds: res.weatherdata.product[0].time[0].location[0].lowClouds[0].$.percent,
          mediumClouds: res.weatherdata.product[0].time[0].location[0].mediumClouds[0].$.percent,
          windSpeed: res.weatherdata.product[0].time[0].location[0].windSpeed[0].$.mps
        },
        count: 0
      })
    })
  .catch(function (error) {
    console.log(error);
  });
    }
  },300)
  }

  componentDidMount(){
    this.getDestinationInfo();
  }
  
  render() {
    const destinationWeatherInfo = this.state.destinationWeatherInfo;
    return (
      <div>
        <ShowDestinationWeatherInfo destinationWeatherInfo= {destinationWeatherInfo} />
      </div>
    );
    }
  }

export default GetDestinationWeatherInfo;
