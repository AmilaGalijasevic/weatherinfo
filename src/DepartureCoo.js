import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class DepartureCoo extends Component {

  static propTypes = {
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
  };

  constructor(props){
    super();
    this.state= {
      departureCoo: {
        lat:'',
        lng:''
      }
    }
  }

  componentWillReceiveProps(departureCoo) {
    this.setState({
      departureCoo: departureCoo.value
    });
  }

  onInputChange = (event) => {
    const name = this.props.name;
    const departureCoo = event.target.value;

    this.setState({
      departureCoo
    });

    this.props.onChange({
      departureCoo,name
    });
  }

  render() {
    return (

      <input
        className='cooInput'
        type="text"
        name='departureLat'
        placeholder= {this.props.placeholder}
        value={this.state.departureLat}
        onChange={this.onInputChange}
        required
        />

    );
  }
}

export default DepartureCoo;
