import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import {Grid, Row, Col } from 'react-bootstrap';

class ShowDestinationWeatherInfo extends Component {

  static propTypes = {
    destinationWeatherInfo: PropTypes.object
  };

  constructor(props){
    super();
    this.state= {
      destinationWeatherInfo: {
        dewPoint:'',
        humidity: '',
        temperature: '',
        fog: '',
        cloudiness:'',
        highClouds: '',
        lowClouds: '',
        mediumClouds: '',
        windSpeed: ''
      }
    }
  }

  componentWillReceiveProps(destinationWeatherInfo) {
    const info = destinationWeatherInfo;
    this.setState({
      destinationWeatherInfo: info.destinationWeatherInfo
    });

  }

  render() {

    const fog= this.state.destinationWeatherInfo.fog;
    const lowClouds= this.state.destinationWeatherInfo.lowClouds;
    const mediumClouds= this.state.destinationWeatherInfo.mediumClouds;
    const highClouds= this.state.destinationWeatherInfo.highClouds;

    return (
      <div>

      <Grid className= "departureGrid animated fadeInRight">
        <Row className="show-grid">
          <Col xs={12} md={6} lg={6}>
            <div className='weatherInfo'>
              <h2>Destination Weather: </h2> <br/>
              <h4>Dew Point: {this.state.destinationWeatherInfo.dewPoint}°</h4>
              <h4>Humidity: {this.state.destinationWeatherInfo.humidity}%</h4>
              <h4>Temperature: {this.state.destinationWeatherInfo.temperature}°</h4>
              <h4>Wind Speed: {this.state.destinationWeatherInfo.windSpeed}mph</h4>

            </div>

          </Col>


          <Col xs={12} md={6} lg={6}>
            <Col className="sky"  lg={3}>
              <ShowFogInfo fog = {fog}/>
            </Col>

            <Col className="sky" md={6} lg={3}>
              <ShowLowCloudsInfo lowClouds = {lowClouds}/>
            </Col>

            <Col className="sky" md={6} lg={3}>
              <ShowMediumCloudsInfo mediumClouds = {mediumClouds}/>
            </Col>

            <Col className="sky" md={6} lg={3}>
              <ShowHighCloudsInfo highClouds = {highClouds}/>
            </Col>

          </Col>
        </Row>
    </Grid>
    </div>
    );
  }
}


function ShowFogInfo(fog){

  if(fog.fog > 0){
    return(
      <div className=''>
        <div className='infoText'>
          <h4>Fog</h4>
          <h5>{fog.fog}%</h5>
        </div>
          <img src={'fog.png'} alt="fog" className="img-responsive fog"/>
      </div>
    );
  }else{
    return(
      <div className='sun'>
      <div className='infoText'>
      <h4>Fog</h4>
      <h5>{fog.fog}%</h5>
      </div>
        <img src={'sun.png'} alt="sun" className="img-responsive sun"/>

      </div>
    );
  }
}

function ShowLowCloudsInfo(lowClouds){

  if(lowClouds.lowClouds > 50){
    return(
      <div className='clouds'>
      <img src={'cloud.png'} alt="cloud" className="img-responsive lowClouds1"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive lowClouds2"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive lowClouds3"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive lowClouds4"/>
      <div className='infoText'>
      <h4>Low Clouds</h4>
      <h5>{lowClouds.lowClouds} %</h5>
      </div>
      </div>
    );
  }

  else if(lowClouds.lowClouds < 20){
      return(
        <div className='infoText'>
        <h4>Low Clouds</h4>
        <h5>{lowClouds.lowClouds} %</h5>
        </div>
      );

    }

  else if(lowClouds.lowClouds > 20 || lowClouds.lowClouds < 50){
    return(
      <div className=''>
      <img src={'cloud.png'} alt="cloud" className="img-responsive lowClouds1"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive lowClouds2"/>
      <div className='infoText'>
      <h4>Low Clouds</h4>
      <h5>{lowClouds.lowClouds} %</h5>
      </div>
      </div>
    );

  }

}

function ShowMediumCloudsInfo(mediumClouds){

  if(mediumClouds.mediumClouds > 50){
    return(
      <div className=''>
      <img src={'cloud.png'} alt="cloud" className="img-responsive mediumClouds1"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive mediumClouds2"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive mediumClouds3"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive mediumClouds4"/>
      <div className='infoText'>
      <h4>Medium Clouds</h4>
      <h5>{mediumClouds.mediumClouds} %</h5>
      </div>
      </div>
    );
  }

  else if(mediumClouds.mediumClouds < 20){
      return(
        <div className='infoText'>
        <h4>Medium Clouds</h4>
        <h5>{mediumClouds.mediumClouds} %</h5>
        </div>
      );

    }

  else if(mediumClouds.mediumClouds > 20 || mediumClouds.mediumClouds < 50){
    return(
      <div className=''>
      <img src={'cloud.png'} alt="cloud" className="img-responsive mediumClouds1"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive mediumClouds2"/>
      <div className='infoText'>
      <h4>Medium Clouds</h4>
      <h5>{mediumClouds.mediumClouds} %</h5>
      </div>
      </div>
    );

  }
}

function ShowHighCloudsInfo(highClouds){

  if(highClouds.highClouds > 50){
    return(
      <div className=''>
      <img src={'cloud.png'} alt="cloud" className="img-responsive highClouds1"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive highClouds2"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive highClouds3"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive highClouds4"/>
      <div className='infoText'>
      <h4>High Clouds</h4>
      <h5>{highClouds.highClouds} %</h5>
      </div>
      </div>
    );
  }

  else if(highClouds.highClouds < 20){
      return(
        <div className='infoText'>
        <h4>High Clouds</h4>
        <h5>{highClouds.highClouds} %</h5>
        </div>
      );

    }

  else if(highClouds.highClouds > 20 || highClouds.highClouds < 50){
    return(
      <div className=''>
      <img src={'cloud.png'} alt="cloud" className="img-responsive highClouds1"/>
      <img src={'cloud.png'} alt="cloud" className="img-responsive highClouds2"/>
      <div className='infoText'>
      <h4>High Clouds</h4>
      <h5>{highClouds.highClouds} %</h5>
      </div>
      </div>
    );

  }


}

export default ShowDestinationWeatherInfo;
