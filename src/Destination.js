import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class Destination extends Component {

  static propTypes = {
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
  }

  constructor(props) {
      super();
      this.state = {
        destination: ''
    }
  }

  componentWillReceiveProps(destinationInput){
    this.setState({
      destination: destinationInput.value
    });
  };

  onInputChange = (event) => {
    const destination = event.target.value;
    const name= this.props.name;
      this.setState({
        destination
       });
       this.props.onChange({
         name,
         destination
       });
    }

  render() {
    return (

      <input
        className='inputs'
        type="text"
        name='destinationInput'
        placeholder={this.props.placeholder}
        value={this.state.destination}
        onChange={this.onInputChange}
        required
        />

    );
  }
}

export default Destination;
