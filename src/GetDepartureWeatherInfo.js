import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import axios from 'axios';
import ShowDepartureWeatherInfo from './ShowDepartureWeatherInfo.js';

class GetDepartureWeatherInfo extends Component {

  static propTypes = {
    departureLocation: PropTypes.object,

  }

  constructor(props) {
      super(props);
      this.state = {
        departureLocation: {
          lat:'',
          lng:''
        },
        departure:'',
        departureWeatherInfo: {
          dewPoint:'',
          humidity: '',
          temperature: '',
          fog: '',
          cloudiness:'',
          highClouds: '',
          lowClouds: '',
          mediumClouds: '',
          windSpeed: ''
        },
        hasError:false,
        _loading:false,
        departureCoordinates:{
          lat:'',
          lng:''
        }

    }
    this.getDepartureInfo= this.getDepartureInfo.bind(this);
  }

  componentWillReceiveProps(data){
    const depLoc = data;
    this.setState({
      departureLocation: {
        lat:depLoc.departureLocation.lat,
        lng:depLoc.departureLocation.lng
      },
    });
      this.getDepartureInfo();
   };

  getDepartureInfo = () => {
    setTimeout(() => {
    var self = this;
    let res;
    const departureLocation = this.state.departureLocation;
    var parseString = require('xml2js').parseString;
    if(!departureLocation.lat==''){
      self.setState({
        _loading:true,
      })
    axios.get(' https://api.met.no/weatherapi/locationforecast/1.9/?lat=' + departureLocation.lat + '&lon='+ departureLocation.lng)
    .then(function (response) {
    var xml = response.data;
    parseString(xml, function (err, result) {
    res= result;
    });
    self.setState({
      departureWeatherInfo: {
        dewPoint:res.weatherdata.product[0].time[0].location[0].dewpointTemperature[0].$.value,
        humidity: res.weatherdata.product[0].time[0].location[0].humidity[0].$.value,
        temperature: res.weatherdata.product[0].time[0].location[0].temperature[0].$.value,
        fog: res.weatherdata.product[0].time[0].location[0].fog[0].$.percent,
        cloudiness: res.weatherdata.product[0].time[0].location[0].cloudiness[0].$.percent,
        highClouds: res.weatherdata.product[0].time[0].location[0].highClouds[0].$.percent,
        lowClouds: res.weatherdata.product[0].time[0].location[0].lowClouds[0].$.percent,
        mediumClouds: res.weatherdata.product[0].time[0].location[0].mediumClouds[0].$.percent,
        windSpeed: res.weatherdata.product[0].time[0].location[0].windSpeed[0].$.mps
      },
      _loading:false,
      hasError: false
    })
  })
    .catch(function (error) {
    console.log(error);
    self.setState({
      hasError: true
      })
    });
    }
  },200)
  }

  componentDidMount(){
    this.getDepartureInfo();
  }

  componentDidCatch(error, info) {
    console.log("catch");
    this.setState({
      hasError:true
    })
  }

  render() {
    const departureWeatherInfo = this.state.departureWeatherInfo;
      return (
      <div>
        <ShowDepartureWeatherInfo
        departureWeatherInfo= {departureWeatherInfo}
        />
      </div>
    );
  }
}

export default GetDepartureWeatherInfo;
