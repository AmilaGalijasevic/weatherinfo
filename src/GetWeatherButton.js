import React, { Component } from 'react';
import './App.css';
import {Button } from 'react-bootstrap';

class GetWeatherButton extends Component {

  render() {
    return (

      <Button
        bsSize="large"
        type='submit'
        value='Submit'
        className="getWeatherButton">
        Submit
        </Button>
    );
  }
}

export default GetWeatherButton;
