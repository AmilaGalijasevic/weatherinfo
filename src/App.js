import React, { Component } from 'react';
import './App.css';
import SubmitLocation from './SubmitLocation.js';

class App extends Component {
  render() {
    return (
      <div className="App">
      <SubmitLocation />
      </div>
    );
  }
}

export default App;
