import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class DestinationCoo extends Component {

  static propTypes = {
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
  };

  constructor(props){
    super();
    this.state= {
      destinationCoo: {
        lat:'',
        lng:''
      }
    }
  }

  componentWillReceiveProps(destinationCoo) {
    this.setState({
      destinationCoo: destinationCoo.value
    });
  }

  onInputChange = (event) => {
    const name = this.props.name;
    const destinationCoo = event.target.value;

    this.setState({
      destinationCoo
    });

    this.props.onChange({
    destinationCoo, name
    });
  }

  render() {
    return (
      <input
        className='cooInput'
        type="text"
        name='destinationlng'
        placeholder= {this.props.placeholder}
        value={this.state.destinationLng}
        onChange={this.onInputChange}
        required
        />



    );
  }
}

export default DestinationCoo;
