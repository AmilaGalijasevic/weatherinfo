import React, { Component } from 'react';
import './App.css';
import Departure from './Departure.js';
import Destination from './Destination.js';
import GetWeatherButton from './GetWeatherButton.js';
import axios from 'axios';
import GetDepartureWeatherInfo from './GetDepartureWeatherInfo.js';
import GetDestinationWeatherInfo from './GetDestinationWeatherInfo.js';
import {Grid, Row, Col } from 'react-bootstrap';
import DepartureCoo from './DepartureCoo.js';
import DestinationCoo from './DestinationCoo.js';



class SubmitLocation extends Component {
  constructor(props){
    super();
    this.state= {
      departure:'',
      destination: '',
      depart: {
      lat: '',
      lng: ''
      },
      dest: {
      lat: '',
      lng: ''
      },
      load: false,
      hasError:false,
      departureCoordinates:{
        lat:'',
        lng:''
      },
      destinationCoordinates: {
        lat: '',
        lng: ''
      },
      coordinates:false

    }

  }

  onDepartureInputChange = ({departure }) => {
    const depart = departure;
    this.setState({
      departure: depart
    });
  };

  onDestinationInputChange = ({destination}) => {
    const dest = destination;
    this.setState({
      destination: dest
    });
  };

  onDepartureCooInputChange = ({name, departureCoo}) => {
    const departureCoordinates= this.state.departureCoordinates;
    departureCoordinates[name]= departureCoo;
    this.setState({
      depart: departureCoordinates
    });


  };

  onDestinationCooInputChange = ({name, destinationCoo }) => {
    const destinationCoordinates = this.state.destinationCoordinates;
    destinationCoordinates[name]= destinationCoo;
    this.setState({
      dest: destinationCoordinates
    });
  };


  onFormSubmit = (event) => {
    const departure = this.state.departure;
    const destination = this.state.destination;
    var self = this;
    this.getCoordsFromGoogleApi(departure)
      .then(data =>{
          self.setState({
            load: true,
            depart:{
              lat: data.data.results[0].geometry.location.lat,
              lng: data.data.results[0].geometry.location.lng
            },
            hasError:false
          })

          this.renderDepartureInfo(data.data.results[0].geometry.location)
        }
      )
      .catch(error => {
        console.log("error " + error)
        this.setState({
          hasError: true
        })
      });


    this.getCoordsFromGoogleApi(destination)
    .then(data =>{
        self.setState({
          load: true,
          dest:{
            lat: data.data.results[0].geometry.location.lat,
            lng: data.data.results[0].geometry.location.lng
          },
          hasError:false
        })
        this.renderDestinationInfo(data.data.results[0].geometry.location)
      }
    )
    .catch(error => {
      console.log("error " + error)
      this.setState({
        hasError: true
      })
    });

    this.setState({
      departure:'',
      destination:'',
      depart:{
        lat:'',
        lng:''
      },
    })
    event.target.reset();
    event.preventDefault();
  }

  onFormSubmitCoordinates = (event) => {

    var self = this;
      if (isNaN(this.state.departureCoordinates.departureLat)
       || isNaN(this.state.departureCoordinates.departureLng)
       || isNaN(this.state.destinationCoordinates.destinationLat)
       || isNaN(this.state.destinationCoordinates.destinationLng))
       {
          self.setState({
            hasError: true
          })
    } else{
      var datadepart= {
        lat: this.state.departureCoordinates.departureLat,
        lng: this.state.departureCoordinates.departureLng
      };
      var datadest= {
        lat: this.state.destinationCoordinates.destinationLat,
        lng: this.state.destinationCoordinates.destinationlng
      };
    self.setState({
      depart:{
        lat: this.state.departureCoordinates.departureLat,
        lng: this.state.departureCoordinates.departureLng
      },
      dest:{
        lat: this.state.destinationCoordinates.destinationLat,
        lng: this.state.destinationCoordinates.destinationLng
      },
      hasError: false
    },
      ()=> this.renderDepartureInfo(datadepart),
      ()=> this.renderDestinationInfo(datadest)
    )
    
    }
    this.renderDepartureInfo();
    this.renderDestinationInfo();
    event.target.reset();
    event.preventDefault();
  }

  getCoordsFromGoogleApi = (name) => {
    return axios.get(
      'https://maps.googleapis.com/maps/api/geocode/json?address='+ name + '&key=AIzaSyBAdK6_AsKrGFNLDyUfDqYL4MF53CI68rw')
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  }

  renderDepartureInfo = (data) =>{

    const departureLocation= {
      lat:this.state.depart.lat,
      lng: this.state.depart.lng
    };

    if(this.state.hasError){
      return(
        <div className='errMsg' >
        <p>Cannot find the requested location, try again </p>
        </div>
      )
    }

    return(
      <div>
        <GetDepartureWeatherInfo
        departureLocation= {departureLocation}
      />
      </div>
    );

  }

  renderDestinationInfo = (data) =>{

    const destinationLocation= {
      lat:this.state.dest.lat,
      lng: this.state.dest.lng
    };

    if(this.state.hasError){
      return(
        <div className='errMsg' >
        <p>Cannot find the requested location, try again </p>
        </div>
      )
    }
    return(

      <div>
        <GetDestinationWeatherInfo
        destinationLocation= {destinationLocation}
      />
      </div>

    );
  }


  componentDidCatch(error, info) {
    console.log("catch");
    this.setState({
      hasErrors:true
    })
  }

  render() {

    return (
      <div>
        <Grid className= "nameInputs animated fadeInLeft">
        <Row>
          <Col xs={12} md={6} lg={6}>
          <form onSubmit={this.onFormSubmit}  >
          <Row className="show-grid">
            <Col xs={12} md={6} lg={6} className='inputCol'>
              <Departure
                placeholder='Departure'
                name='departure'
                value={this.state.departure}
                onChange={this.onDepartureInputChange}/>
            </Col>
           </Row>
           <Row className="show-grid">
             <Col xs={12} md={6} lg={6} className='inputCol'>
              <Destination
                placeholder='Destination'
                name='destination'
                value={this.state.destination}
                onChange={this.onDestinationInputChange}
               />
              </Col>
           </Row>
           <Row className="show-grid">
             <Col xs={12} md={10} lg={10} className='inputCol'>
              <GetWeatherButton/>
             </Col>
          </Row>
          </form>
        </Col>
        <Col xs={12} md={6} lg={6}>
        <form onSubmit={this.onFormSubmitCoordinates}>
        <Row className="show-grid">
          <Col xs={12} md={6} lg={6} className='inputCol'>
            <DepartureCoo
              placeholder='Departure Lat'
              name='departureLat'
              value={this.state.departureLat}
              onChange={this.onDepartureCooInputChange}/>
          </Col>
          <Col xs={12} md={6} lg={6} className='inputCol'>
          <DepartureCoo
            placeholder='Departure Lng'
            name='departureLng'
            value={this.state.departureLng}
            onChange={this.onDepartureCooInputChange}/>
           </Col>
         </Row>
         <Row className="show-grid">
           <Col xs={12} md={6} lg={6} className='inputCol'>
           <DestinationCoo
             placeholder='Destination Lat'
             name='destinationLat'
             value={this.state.destinationLat}
             onChange={this.onDestinationCooInputChange}/>
            </Col>
            <Col xs={12} md={6} lg={6} className='inputCol'>
            <DestinationCoo
              placeholder='Destination Lng'
              name='destinationLng'
              value={this.state.destinationLng}
              onChange={this.onDestinationCooInputChange}/>
             </Col>
         </Row>
         <Row className="show-grid">
           <Col xs={12} md={12} lg={12} className='inputCol inputColButton'>
            <GetWeatherButton/>
           </Col>
          </Row>
        </form>
        </Col>

      </Row>
      </Grid>

      <div>


      </div>



          <div>
            <div>{this.renderDepartureInfo()}</div>
            <div>{this.renderDestinationInfo()}</div>
          </div>

      </div>
    );
  }
}


export default SubmitLocation;
